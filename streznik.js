var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


app.get('/api/dodaj', function(req, res) {
	var ds = req.param('ds');
	var ime = req.param('ime');
	var priimek = req.param('priimek');
	var ulica = req.param('ulica');
	var hisnaStevilka = req.param('hisnaStevilka');
	var postnaStevilka = req.param('postnaStevilka');
	var kraj = req.param('kraj');
	var drzava = req.param('drzava');
	var poklic = req.param('poklic');
	var telefonskaStevilka = req.param('telefonskaStevilka');
	if(ds && ime && priimek && ulica && hisnaStevilka && kraj && drzava && poklic && telefonskaStevilka) {
		//preveri ce oseba ze obstaja v spominu
		var oseba = {davcnaStevilka: ds, ime: ime, priimek: priimek, naslov: ulica, hisnaStevilka: hisnaStevilka, postnaStevilka: postnaStevilka, kraj: kraj, drzava: drzava, poklic: poklic, telefonskaStevilka: telefonskaStevilka};

		//dodaj osebo v spomin

		res.redirect();
	}
	else {
		res.send("Napaka pri dodajanju osebe!");
	}
});


app.get('/api/brisi', function(req, res) {
	var davcnaStevilka = req.param('id');
	if(davcnaStevilka == null || davcnaStevilka.length == 0) {
		return res.send("Napačna zahteva!");
	}
	var index = poisciIndexUpoarbnika(davcnaStevilka);
	if(index > 0) {
		uporabnikiSpomin = uporabnikiSpomin.splice(index, 1);
		res.redirect('back');
	}
	else {
		res.send("Oseba z davčno številko "+davcnaStevilka+" ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
	}
});

function poisciIndexUpoarbnika(davcnaStevilka){
	for(var i = 0; i < uporabnikiSpomin.length; i++) {
		if(uporabnikiSpomin[i].davcnaStevilka == davcnaStevilka) {
			return i;
		}
	}
	return 0;
}

var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];